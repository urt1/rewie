package router

import (
	"net/http"
	"rewie/internal/handler"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
)

func NewApiRouter(controllers *handler.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/get", controllers.GetList)
	r.Delete("/delete/{id}", controllers.DeleteUser)
	r.Post("/create", controllers.CreateUsert)
	r.Post("/update/{id}", controllers.UpdateUser)
	return r
}
