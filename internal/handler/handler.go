package handler

import (
	"encoding/json"
	"net/http"
	"rewie/internal/modules"
	"strconv"

	"github.com/go-chi/chi/v5"
)

type service interface {
	GetList() ([]modules.Users, error)
	Update(user modules.Users, id int) (modules.Users, error)
	Delete(id int) error
	Create(user modules.Users) error
}
type Handler struct {
	servise service
}

func NewServerController(u service) Handler {
	return Handler{u}
}
func (h *Handler) GetList(w http.ResponseWriter, r *http.Request) {
	list, err := h.servise.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(list)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) CreateUsert(w http.ResponseWriter, r *http.Request) {
	var user modules.Users
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.servise.Create(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	userIDRaw := chi.URLParam(r, "id")

	userID, err := strconv.Atoi(userIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.servise.Delete(userID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {

	userIDRaw := chi.URLParam(r, "id")

	userID, err := strconv.Atoi(userIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var user modules.Users
	err = json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.servise.Update(user, userID)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(res)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
