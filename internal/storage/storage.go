package storage

import (
	"rewie/internal/modules"

	"github.com/jmoiron/sqlx"
)

type Storage struct {
	sqlDb *sqlx.DB
}

func NewStorage(db *sqlx.DB) (*Storage, error) {
	query := "CREATE TABLE IF NOT EXISTS Users (id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,first_name VARCHAR(50),last_name VARCHAR(50), age INT, email VARCHAR(100), phone VARCHAR(20), address VARCHAR(100), city VARCHAR(50), country VARCHAR(50) );"
	_, err := db.Exec(query)
	res := &Storage{db}
	return res, err
}

func (r *Storage) Create(user modules.Users) error {
	query := "INSERT INTO users(first_name,last_name,age,email,phone,address,city,country) VALUES(?,?,?,?,?,?,?,?)"
	_, err := r.sqlDb.Exec(query, user.Name, user.LastName, user.Age, user.Email, user.Phone, user.Addres, user.City, user.Country)
	return err
}

func (r *Storage) GetList() ([]modules.Users, error) {
	var res []modules.Users
	err := r.sqlDb.Select(&res, "SELECT * FROM users")
	return res, err
}

func (r *Storage) Update(user modules.Users) error {
	query := `update 
	users 
  set 
	first_name = ?,
	last_name = ?,
	age = ?,
	email = ?,
	phone = ?,
	address = ?,
	city = ?,
	country = ?
  where 
	id = ?;`
	_, err := r.sqlDb.Exec(query, user.Name, user.LastName, user.Age, user.Email, user.Phone, user.Addres, user.City, user.Country, user.ID)
	return err
}

func (r *Storage) GetById(id int) (modules.Users, error) {
	var res modules.Users
	row, err := r.sqlDb.Query(`SELECT * FROM users WHERE "id" = ? LIMIT 1 `, id)
	row.Scan(&res)
	return res, err
}

func (r *Storage) Delete(user modules.Users) error {
	query := `delete from 
	users 
  where 
	id = ?;`
	_, err := r.sqlDb.Exec(query, user.ID)
	return err
}
