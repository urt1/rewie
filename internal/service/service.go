package service

import "rewie/internal/modules"

type storages interface {
	Create(user modules.Users) error
	GetList() ([]modules.Users, error)
	Update(user modules.Users) error
	GetById(id int) (modules.Users, error)
	Delete(user modules.Users) error
}

type Service struct {
	storage storages
}

func NewService(str storages) *Service {
	return &Service{str}
}

func (s *Service) GetList() ([]modules.Users, error) {
	res, err := s.storage.GetList()

	return res, err
}

func (s *Service) Update(user modules.Users, id int) (modules.Users, error) {
	err := s.storage.Update(user)
	if err != nil {
		return modules.Users{}, err
	}
	res, err := s.storage.GetById(id)
	return res, err
}
func (s *Service) Delete(id int) error {
	user, err := s.storage.GetById(id)
	if err != nil {
		return err
	}
	err = s.storage.Delete(user)
	return err
}
func (s *Service) Create(user modules.Users) error {
	err := s.storage.Create(user)
	return err
}
