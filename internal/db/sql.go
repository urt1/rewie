package db

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq"
)

func NewSqlDB() (*sqlx.DB, error) {
	var dsn string
	var err error
	var dbRaw *sql.DB
	dsn = "host=localhost port=5432 user=postgres password=root dbname=postgres sslmode=disable"
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * 5)

	for {
		select {
		case <-timeoutExceeded:
			return nil, fmt.Errorf("db connection failed after %d timeout %s", 5, err)
		case <-ticker.C:
			dbRaw, err = sql.Open("postgres", dsn)
			if err != nil {
				return nil, err
			}
			err = dbRaw.Ping()
			if err == nil {
				db := sqlx.NewDb(dbRaw, "postgres")
				db.SetMaxOpenConns(50)
				db.SetMaxIdleConns(50)
				return db, nil
			}
		}
	}
}
