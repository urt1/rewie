package modules

type Users struct {
	ID       int    `json:"id" db:"id"`
	Name     string `json:"name" db:"first_name"`
	LastName string `json:"last_name" db:"last_name"`
	Age      int    `json:"age" db:"age"`
	Email    string `json:"email" db:"email"`
	Phone    string `json:"phone" db:"phone"`
	Addres   string `json:"address" db:"address"`
	City     string `json:"city" db:"city"`
	Country  string `json:"country" db:"country"`
}
