package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"rewie/internal/db"
	"rewie/internal/handler"
	"rewie/internal/router"
	"rewie/internal/service"
	"rewie/internal/storage"
	"time"
)

func main() {
	port := ":8080"
	sql, err := db.NewSqlDB()
	if err != nil {
		panic(err)
	}
	storage, _ := storage.NewStorage(sql)

	controller := handler.NewServerController(service.NewService(storage))
	r := router.NewApiRouter(&controller)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
